package main

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/nolith/mqc-aprs/pkg/mqc"
)

func main() {
	ctx := context.Background()

	ping(ctx)

	spots(ctx)

	last(ctx)
}

func ping(ctx context.Context) {
	ctx2, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	pong, err := mqc.DefaultClient.Ping(ctx2)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\n\n%v\n", pong)
}

func last(ctx context.Context) {
	ctx2, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	spot, err := mqc.DefaultClient.GetLastSpot(ctx2)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\n\n%v\n", spot)
}

func spots(ctx context.Context) {
	ctx2, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	spot, err := mqc.DefaultClient.GetSpots(ctx2, 3)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\n\n%v\n", spot)
}
