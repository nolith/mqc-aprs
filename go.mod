module gitlab.com/nolith/mqc-aprs

go 1.16

require (
	github.com/apex/log v1.9.0
	github.com/golang/geo v0.0.0-20210211234256-740aa86cb551 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.2 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tb0hdan/go-aprs v0.0.0-20191117180800-a397a20fa94b
)
