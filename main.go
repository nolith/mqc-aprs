package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	// "gitlab.com/nolith/mqc-aprs/pkg/mqc"
	// "gitlab.com/nolith/mqc-aprs/pkg/tracker"

	"gitlab.com/nolith/mqc-aprs/pkg/aprsis"
)

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	log.SetHandler(cli.Default)

	logger := log.WithField("app", "mqc-aprs")

	router := mux.NewRouter()

	// Prometheus endpoint
	router.Path("/metrics").Handler(promhttp.Handler())

	ctx := log.NewContext(context.Background(), logger)
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	err := aprsis.Start(
		ctx,
		os.Getenv("APRS_CALLSIGN"),
		os.Getenv("APRS_PASSWORD"))
	if err != nil {
		logger.WithError(err).Fatal("Start failed")
	}

	notifyOnBoot := os.Getenv("NOTIFY_ON_BOOT")
	if notifyOnBoot != "" {
		aprsis.SendMessageTo(notifyOnBoot, "Booting now!")
	}

	go func() {
		select {
		case <-ctx.Done():
			logger.Warn("Shutting down")
		case sig := <-sigs:
			logger.WithField("signal", sig).Warn("Signal received")
			cancel()
		}

	}()

	go func() {
		logger.WithField("port", 9000).Info("Serving incoming requests")
		err = http.ListenAndServe(":9000", router)
		logger.WithError(err).Warn("HTTP endpoint failure")

		//cancel()
	}()

	aprsis.Wait()
}
