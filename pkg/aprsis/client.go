package aprsis

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/apex/log"
	"github.com/tb0hdan/go-aprs"
	"github.com/tb0hdan/go-aprs/aprsis"

	"gitlab.com/nolith/mqc-aprs/pkg/metrics"
	"gitlab.com/nolith/mqc-aprs/pkg/mqc"
)

type client struct {
	ctx      context.Context
	callsign string
	client   *aprsis.APRSIS

	outbox chan *aprs.Frame
	done   sync.WaitGroup
}

var defaultClient *client

const (
	outboxBufferSize = 5

	maxMessageLen = 67

	msgProcessingTimeoutSec = 5
)

func Start(ctx context.Context, callsign, password string) error {
	defaultClient = &client{
		ctx:      ctx,
		callsign: callsign,
		outbox:   make(chan *aprs.Frame, outboxBufferSize),
	}

	filter := fmt.Sprintf("u/%s", callsign)
	client, err := aprsis.NewAPRS(
		callsign,
		password,
		filter)
	if err != nil {
		defaultClient.log().WithError(err).Fatal("Login failed")
		defaultClient = nil

		return err
	}

	defaultClient.log().
		WithField("callsign", callsign).
		WithField("filter", filter).
		Info("Connected to APRS-IS")

	defaultClient.client = client

	defaultClient.done.Add(2)
	go defaultClient.mainLoop()
	go defaultClient.sendLoop()

	return nil
}

func Wait() {
	defaultClient.done.Wait()
}

func (c *client) log() log.Interface {
	return log.FromContext(c.ctx)
}

func (c *client) sendLoop() {
	for {
		select {
		case <-c.ctx.Done():
			c.log().Warn("Terminating the send loop")
			c.done.Done()
			return

		case packet := <-c.outbox:
			metrics.PacketsSent.Inc()
			err := c.client.SendPacket(*packet)
			if err != nil {
				metrics.PacketsSentErrors.Inc()

				c.log().WithError(err).Error("Send packet failed")
			}
		}
	}
}

func (c *client) mainLoop() {
	defer c.client.Close()

	go c.client.ManageConnection()

	for {
		select {
		case <-c.ctx.Done():
			c.log().Warn("Terminating the receive loop")

			c.done.Done()
			return
		case frame := <-c.client.GetIncomingMessages():
			metrics.FrameProcessed.Inc()

			c.log().WithField("from", frame.Source.String()).WithField("to", frame.Dest.Call).Info(frame.Body.Type().String())
			c.log().Trace(frame.String())

			if frame.Body.Type().IsMessage() {
				msg := frame.Message()

				if msg.Recipient.String() != c.callsign {
					c.log().WithField("recipient", msg.Recipient.String()).Warn("Received a message for another station")
					continue
				}

				c.processMessage(&msg)
			}

		}
	}
}

func (c *client) processMessage(msg *aprs.Message) {
	ctx, cancel := context.WithTimeout(c.ctx, msgProcessingTimeoutSec*time.Second)
	defer cancel()

	from := msg.Sender.String()
	to := msg.Recipient.String()

	c.log().WithField("from", from).WithField("to", to).WithField("id", msg.ID).Info(msg.Body)

	c.ackMessage(msg)

	if shouldStop(ctx) {
		c.log().Warn("Processing time expired")

		return
	}

	switch strings.ToLower(msg.Body) {
	case "?":
		c.sendMessageTo(from, "last - Mountain QRP Club ultimo spot")
		c.sendMessageTo(from, "spots - Mountain QRP Club ultimi 5 spot")
	case "last":
		spot, err := mqc.DefaultClient.GetLastSpot(ctx)
		if err != nil {
			c.log().WithError(err).Warn("GetLastSpot failed")
			return
		}

		if spot == nil {
			c.sendMessageTo(from, "Nessuno spot recente sul cluster MQC")
			return
		}

		c.sendMessageTo(from, spot.String())
	case "spots":
		spots, err := mqc.DefaultClient.GetSpots(ctx, 5)
		if err != nil {
			c.log().WithError(err).Warn("GetLastSpot failed")
			return
		}

		if len(spots) == 0 {
			c.sendMessageTo(from, "Nessuno spot recente sul cluster MQC")
			return
		}

		for _, spot := range spots {
			c.sendMessageTo(from, spot.String())
		}
	}
}

func shouldStop(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
		return false
	}
}

func (c *client) address() aprs.Address {
	return aprs.AddressFromString(c.callsign)
}

func (c *client) ackMessage(msg *aprs.Message) {
	if msg.ID == "" {
		return
	}

	ack := &aprs.Message{
		Sender:    c.address(),
		Recipient: msg.Sender,
		Body:      fmt.Sprintf("ack%s", msg.ID),
	}

	c.sendMessage(ack)
}

func (c *client) sendMessage(msg *aprs.Message) {
	packet := &aprs.Frame{
		Source: msg.Sender,
		Dest:   msg.Recipient,
		Path: []aprs.Address{
			{Call: "TCPIP*"}, {Call: "qAC"},
		},
		Body: aprs.Info(msg.String()),
	}

	c.outbox <- packet
}

func (c *client) sendMessageTo(recipient, message string) {
	if len(message) > maxMessageLen {
		cutIdx := maxMessageLen - 3
		first := message[0:cutIdx] + "..."
		c.sendMessageTo(recipient, first)
		c.sendMessageTo(recipient, message[cutIdx+1:])

		return
	}

	msg := &aprs.Message{
		Sender:    c.address(),
		Recipient: aprs.AddressFromString(recipient),
		Body:      message,
	}

	c.sendMessage(msg)
}

func SendMessageTo(recipient, message string) {
	defaultClient.sendMessageTo(recipient, message)
}
