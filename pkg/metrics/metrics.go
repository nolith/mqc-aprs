package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	FrameProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "aprs_frame_total",
		Help: "The total number of processed frames",
	})

	PacketsSent = promauto.NewCounter(prometheus.CounterOpts{
		Name: "aprs_packet_sent_total",
		Help: "The total number of packets sent",
	})

	PacketsSentErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "aprs_packet_sent_error_total",
		Help: "The total number of errors sendind packets",
	})
)
