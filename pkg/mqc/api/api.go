package api

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

var (
	idFaucet = make(chan string)

	initFaucet sync.Once
)

const (
	apiDateFormat   = "2006-01-02 15:04:05"
	shortTimeFormat = "15:04"
)

type action string

type Request struct {
	Id      string                 `json:"id"`
	Action  action                 `json:"action"`
	Token   string                 `json:"token"`
	Content map[string]interface{} `json:"content"`
}

type Answer struct {
	Id           string      `json:"id"`
	Result       bool        `json:"result"`
	Content      interface{} `json:"content"`
	ErrorMessage string      `json:"errorMessage"`
}

func (a *Answer) Error() error {
	if a.ErrorMessage == "" {
		return nil
	}

	return errors.New(a.ErrorMessage)
}

type Spot struct {
	Date    string `mapstructure:"data"`     //data e ora dello spot
	Call    string `mapstructure:"call"`     //call di chi ha inviato lo spot,
	CallDX  string `mapstructure:"calldx"`   //call di chi sta effettuando la chiamata o l’attivazione
	Freq    string `mapstructure:"freq"`     // frequenza in KHz
	Locator string `mapstructure:"wwl"`      //locatore,
	Comment string `mapstructure:"commento"` // commento,
	Ref     string `mapstructure:"ref"`      // referenza in base al diploma MQC
}

func (s *Spot) When() time.Time {
	loc, _ := time.LoadLocation("Europe/Rome")

	t, err := time.ParseInLocation(apiDateFormat, s.Date, loc)
	if err != nil {
		t = time.Now()
	}

	return t.UTC()
}

func (s *Spot) Where() string {
	if s.Ref != "" {
		return s.Ref
	}

	return s.Locator
}

func (s *Spot) String() string {
	return fmt.Sprintf("%sz %s at %s %sKHz %s",
		s.When().Format(shortTimeFormat),
		s.CallDX,
		s.Where(),
		s.Freq,
		s.Comment)
}

func getID() string {
	initFaucet.Do(func() {
		go func() {
			idx := 1
			for {
				idFaucet <- fmt.Sprintf("%04d", idx)

				idx++
			}
		}()
	})

	return <-idFaucet
}

func Ping() Request {
	return Request{
		Id:     getID(),
		Action: action("PING"),
	}
}

func GetLastSpot() Request {
	return Request{
		Id:     getID(),
		Action: action("GET_LASTSPOT"),
	}
}

func GetSpots(nr uint) Request {
	return Request{
		Id:     getID(),
		Action: action("GET_SPOTS"),
		Content: map[string]interface{}{
			"nr": nr,
		},
	}
}
