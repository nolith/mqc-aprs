package mqc

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/nolith/mqc-aprs/pkg/mqc/api"
)

var DefaultClient = &Client{
	Endpoint: "https://www.mountainqrp.it/report/wp/api/MQCServices.php",
	Token:    os.Getenv("MQC_TOKEN"),
}

type Client struct {
	Endpoint string
	Token    string
}

func (c *Client) Ping(ctx context.Context) (*api.Answer, error) {
	msg := api.Ping()
	return c.do(ctx, &msg)
}

func (c *Client) GetLastSpot(ctx context.Context) (*api.Spot, error) {
	msg := api.GetLastSpot()

	ans, err := c.do(ctx, &msg)
	if err != nil {
		return nil, err
	}

	// This API returns an empty array when no spot are found
	if _, ok := ans.Content.([]interface{}); ok {
		return nil, nil
	}

	spot := new(api.Spot)
	err = mapstructure.Decode(ans.Content, spot)

	return spot, err
}

func (c *Client) GetSpots(ctx context.Context, nr uint) ([]api.Spot, error) {
	msg := api.GetSpots(5)

	ans, err := c.do(ctx, &msg)
	if err != nil {
		return nil, err
	}

	rawSpots, ok := ans.Content.([]interface{})
	if !ok {
		return nil, errors.New("cannot extract spots")
	}

	spots := make([]api.Spot, len(rawSpots))
	for i, rawSpot := range rawSpots {
		if err := mapstructure.Decode(rawSpot, &spots[i]); err != nil {
			return nil, err
		}
	}

	return spots, nil
}

func (c *Client) do(ctx context.Context, msg *api.Request) (*api.Answer, error) {
	msg.Token = c.Token

	r, w := io.Pipe()
	enc := json.NewEncoder(w)
	teeInput := io.TeeReader(r, os.Stdout)

	go func() {
		enc.Encode(msg)
		w.Close()
	}()

	req, err := http.NewRequestWithContext(ctx, "POST", c.Endpoint, teeInput)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Encoding", "application/json")
	req.Header.Set("Accept", "application/json")

	os.Stdout.WriteString("-> ")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("Status: %s", resp.Status)
	}

	defer resp.Body.Close()
	teeOutput := io.TeeReader(resp.Body, os.Stdout)
	dec := json.NewDecoder(teeOutput)
	answer := new(api.Answer)

	os.Stdout.WriteString("<- ")
	if err := dec.Decode(answer); err != nil {
		return nil, err
	}

	return answer, answer.Error()
}
