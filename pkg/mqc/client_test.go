package mqc_test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/nolith/mqc-aprs/pkg/mqc"
)

func TestGetLastSpot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `{"result":true,"id":"0003","content":{"data":"2021-11-02 20:23:10","call":"IQ3QC","calldx":"IU5BON","freq":"145500","wwl":"JN53OT","commento":"test spot","ref":"I-0001"}}`)
	}))
	defer ts.Close()

	client := mqc.Client{Endpoint: ts.URL}
	spot, err := client.GetLastSpot(context.Background())
	require.NoError(t, err)
	require.NotNil(t, spot)

	assert.Equal(t, "IU5BON", spot.CallDX)
	assert.Equal(t, "2021-11-02 20:23:10", spot.Date)
	assert.Equal(t, "IQ3QC", spot.Call)
	assert.Equal(t, "145500", spot.Freq)
	assert.Equal(t, "JN53OT", spot.Locator)
	assert.Equal(t, "test spot", spot.Comment)
	assert.Equal(t, "I-0001", spot.Ref)

}

func TestEmptyGetLastSpot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `{"result":true,"id":"0003","content":[]}`)
	}))
	defer ts.Close()

	client := mqc.Client{Endpoint: ts.URL}
	spot, err := client.GetLastSpot(context.Background())
	require.NoError(t, err)
	require.Nil(t, spot)
}

func TestEmptyGetSpots(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `{"result":true,"id":"0004","content":[]}`)
	}))
	defer ts.Close()

	client := mqc.Client{Endpoint: ts.URL}
	spots, err := client.GetSpots(context.Background(), 1)
	require.NoError(t, err)
	require.NotNil(t, spots)
	require.Empty(t, spots)
}

func TestGetSpots(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, `{"result":true,"id":"0004","content":[{"data":"2021-11-02 20:23:10","call":"IQ3QC","calldx":"IU5BON","freq":"145500","wwl":"JN53OT","commento":"test spot","ref":"I-0001"}]}`)
	}))
	defer ts.Close()

	client := mqc.Client{Endpoint: ts.URL}
	spots, err := client.GetSpots(context.Background(), 1)
	require.NoError(t, err)
	require.NotNil(t, spots)
	require.NotEmpty(t, spots)

	spot := spots[0]

	assert.Equal(t, "IU5BON", spot.CallDX)
	assert.Equal(t, "2021-11-02 20:23:10", spot.Date)
	assert.Equal(t, "IQ3QC", spot.Call)
	assert.Equal(t, "145500", spot.Freq)
	assert.Equal(t, "JN53OT", spot.Locator)
	assert.Equal(t, "test spot", spot.Comment)
	assert.Equal(t, "I-0001", spot.Ref)
	assert.Equal(t, "19:23z IU5BON at I-0001 145500KHz test spot", spot.String())
}
